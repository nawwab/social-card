import React, {Component} from 'react'
import Card from './Card'

class App extends Component {
    render(){
        // const cardData = [
        //     {
        //         img: "img/hold-on.jpg",
        //         header: "Hold On!",
        //         par:   `Kucoba 'tuk bertahan
        //             Dalam kisah ini
        //             Tak bisakah sejenak
        //             Kau jangan pergi`,
        //     },
        //     {
        //         img: "img/images.jpg",
        //         header: "One Eyes",
        //         par:   `Im just an eye, please
        //             dont call me illuminatti and
        //             conspiracy`,
        //     },
        //     {
        //         img: "img/Jsmeme.png",
        //         header: "Javascript",
        //         par:   `hey kids, be aware of 
        //                 javascript`,
        //     },
        //     {
        //         img: "img/lisa.jpg",
        //         header: "Kenakalan di-era informatika",
        //         par:   `Jangan buka aneh2 ya hei kamu`,
        //     },
        //     {
        //         img: "img/me-and-the-boys.jpg",
        //         header: "College",
        //         par:   `College is so f* sucks,
        //                 i hate my university`,
        //     },
        //     {
        //         img: "img/Jsmeme.png",
        //         header: "Javascript",
        //         par:   `hey kids, be aware of 
        //                 javascript`,
        //     },
        // ]

        const datas = [
            {
                img: require("./img/images.jpg"),
                header: "One Eyes",
                par:   `Im just an eye, please
                dont call me illuminatti and
                conspiracy or kill me`,
            },
            {
                img: require("./img/lisa.jpg"),
                header: "Hello Gais",
                par:   `Yo Whatsapp kembali lagi
                sama eug youtuber kece`,
            },
            {
                img: require("./img/hold-on.JPG"),
                header: "JS sucks",
                par:   `please hold-on, jangan kemana-mana
                tetap di Opera Van Java Ya'e`,
            },
        ]
        
        return(
            <div className="card-wrapper">
                {datas.map((val) => <Card data = { val } />)}
            </div>
        )
    }
}

export default App