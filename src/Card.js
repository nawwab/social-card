import React, { Component } from 'react'

const Cover = (props) => {
    return(
        <img src={props.image.img} alt="card-images" />
    )
}

const Header = (props) => {
    return(
        <h1>{props.title.header}</h1>
    )
}

const Par = (props) => {
    return(
        <p>{props.content.par}</p>
    )
}

class Card extends Component{
    render() {
        const { data } = this.props

        return(
            <div className="Card">
                <div className="img-wrapper">
                    <Cover image = {data} />
                </div>
                <div className="desc">
                    <Header title = {data} />
                    <Par content = {data} />
                </div>
            </div>
        )
    }
}

export default Card